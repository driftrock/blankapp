require 'spec_helper'

describe FactoryGirl do
  FactoryGirl.factories.each do |factory|
    context "with factory for :#{factory.name}" do
      subject {FactoryGirl.build(factory.name)}

      it "is valid" do
        subject.valid?.should be, subject.errors.full_messages.join(',')
      end

      context "traits" do
        factory.defined_traits.each do |trait|
          describe "trait: #{trait.name}" do
            subject {FactoryGirl.build(factory.name, trait.name)}

            it "is valid" do
              subject.valid?.should be, subject.errors.full_messages.join(',')
            end
          end
        end
      end
    end
  end
end
