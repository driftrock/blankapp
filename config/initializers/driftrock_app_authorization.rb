Driftrock::AppAuthorization.configure do |config|
  config.website_url = Rails.application.secrets.website_url
  config.app_path    = 'my-app-path'
  config.app_secret  = Rails.application.secrets.app_salt
  config.root_path   = '/'
end
