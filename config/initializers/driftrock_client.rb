Driftrock::Client.configure do |config|
  config.version  = 'v1'
  config.base_url = Rails.application.secrets.api_url
  config.logger   = Rails.logger
end
