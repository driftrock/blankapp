class ApplicationController < ActionController::Base
  include Driftrock::AuthorizationConcerns
  # Gives access to method require_authorization!
  # which can be used in a before_filter
  include Driftrock::AppConcerns
  # Gives access to helper methods:
  # helper_method :app_info # Returns information about the current app
  # helper_method :current_user # Returns information about the current user
  # helper_method :current_company # Returns information about the current company
  # helper_method :current_subscription # Returns information about the current subscription the user is on for this app
  protect_from_forgery with: :exception
  before_filter :require_authorization!

  def driftrock_client
    # This session variable is set by the authorization gem
    @driftrock_client ||= Driftrock::Client.new(headers: {
      access_token: session[:access_token]
    }) 
  end

  # Example use of driftrock client
  def current_user_as_hash
    driftrock_client.get('/user')
  end
end
