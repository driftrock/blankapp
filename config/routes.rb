Rails.application.routes.draw do
  mount DriftrockAppAuthorization::Engine => "/driftrock"
end
